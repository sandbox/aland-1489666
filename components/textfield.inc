<?php

/**
 * @file
 * Webform Encrypted TextField module textfield component.
 *
 * Implemented _webform_[callback]_[component] callbacks:
 *   - defaults
 *   - theme
 *   - render
 *   - analysis
 *   - display
 *   - csv_headers
 *   - csv_data
 *   - submit
 *   - table
 *   - edit
 *
 * Unimplemented callbacks:
 *   - delete
 *   - help
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_wec_textfield() {
  module_load_include('inc', 'webform', 'components/textfield');
  $defaults = _webform_defaults_textfield();
  $defaults['extra']['see_own_values'] = 1;
  $defaults['extra']['roles'] = array();
  $defaults['extra']['encrypt_method'] = variable_get('encrypt_default_method', ENCRYPT_DEFAULT_METHOD);
  return $defaults;
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_wec_textfield() {
  return array(
    'webform_display_wec_textfield' => array(
      'arguments' => array('element' => NULL),
      'file' => 'components/textfield.inc',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_wec_textfield($component) {
  module_load_include('inc', 'webform', 'components/textfield');
  $form = _webform_edit_textfield($component);


  // Initialize to just see if any funny business is going on
  encrypt_initialize();

  // Run requirements (include install file first)
  module_load_include('install', 'encrypt', 'encrypt');
  $requirements = encrypt_requirements('runtime');
  // Give some help if keys are not found
  if ($requirements['encrypt_keys']['severity'] == REQUIREMENT_ERROR) {
    // Let user know that key is not secure
    drupal_set_message(t('A problem was found with your secure key.') . ' ' . $requirements['encrypt_keys']['value'],  'warning');
  }

  // Get formatted methods
  $methods = theme('encrypt_admin_list', encrypt_get_methods('full', TRUE));

  // Define the encyption method.
  // @todo: More help to ensure that users actually do this correctly.
  // The encypt module supports
  $form['encrypt_method'] = array(
    '#type' => 'radios',
    '#title' => t('Encryption method'),
    '#description' => t('Define the encryption method used for this component.'),
    '#options' => $methods,
    '#default_value' => $component['extra']['encrypt_method'],
  );

  // @todo: Implement these options.
  $form['see_own_values'] = array(
    '#type' => 'checkbox',
    '#title' => t('See own submitted data'),
    '#description' => t('This allows the user that submitted the form to see their own submission.'),
    '#default_value' => $component['extra']['see_own_values'],
    '#access' => FALSE,
  );

  $roles = user_roles(TRUE);
  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User roles that are allowed to see the data'),
    '#description' => t('This only applies if the user can see the submitted data, etc. Uncheck all to bypass this restriction.'),
    '#options' => $roles,
    '#default_value' => $component['extra']['roles'],
    '#access' => FALSE,
  );

  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_wec_textfield($component, $value = NULL, $filter = TRUE) {
  module_load_include('inc', 'webform', 'components/textfield');
  $value[0] = empty($value[0]) ? '' : decrypt($value[0]);
  $element = _webform_render_textfield($component, $value, $filter);
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_wec_textfield($component, $value, $format = 'html') {
  module_load_include('inc', 'webform', 'components/textfield');
  $value[0] = empty($value[0]) ? '' : decrypt($value[0]);
  $element = _webform_display_textfield($component, $value, $format);
  return $element;
}

/**
 * Format the output of data for this component.
 */
function theme_webform_display_wec_textfield($element) {
  return theme('webform_display_textfield', $element);
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_wec_textfield($component, $sids = array()) {
  module_load_include('inc', 'webform', 'components/textfield');
  $results = _webform_analysis_textfield($component, $sids);
  array_pop($results);
  return $results;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_wec_textfield($component, $value) {
  $encrypt_method = $component['extra']['encrypt_method'];
  $value[0] = empty($value[0]) ? '' : decrypt($value[0]);
  return check_plain($value[0]);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_wec_textfield($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_wec_textfield($component, $export_options, $value) {
  return empty($value[0]) ? '' : decrypt($value[0]);
}

/**
 * A hook for changing the input values before saving to the database.
 */
function _webform_submit_wec_textfield($component, $value) {
  if (!empty($value)) {
    $encrypt_method = $component['extra']['encrypt_method'];
    return encrypt($value, array('base64' => TRUE), $encrypt_method);
  }
  return '';
}

